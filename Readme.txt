# Version 1.0
# Author Nick Shaw - www.alwaysnetworks.co.uk

"""
Script to take a CSV from CA's eHealth, detailing the bandwidth utilisation of an interface (bits in and bits out), and produce an output CSV containing the 95th percentile per day.
The data format from eHealth is as follows:
"eHealth Trend Report","logoRpt"
"DEVICENAME-INTERFACE NAME"
"Divide by Time"

"","","Bandwidth Utilization In","Bandwidth Utilization Out"
"Sample Time","Delta Time","%","%"
"30/06/2015 06:00:35 PM",316,5.15666389,31.84219170
"30/06/2015 06:05:52 PM",317,5.22666550,32.36996078
......etc......
"""

