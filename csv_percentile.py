#!/usr/bin/python
# Version 1.0
# Author Nick Shaw - www.alwaysnetworks.co.uk

"""
Script to take a CSV from CA's eHealth, detailing the bandwidth utilisation of an interface (bits in and bits out), and produce an output CSV containing the 95th percentile per day.
The data format from eHealth is as follows:
"eHealth Trend Report","logoRpt"
"DEVICENAME-INTERFACE NAME"
"Divide by Time"

"","","Bandwidth Utilization In","Bandwidth Utilization Out"
"Sample Time","Delta Time","%","%"
"30/06/2015 06:00:35 PM",316,5.15666389,31.84219170
"30/06/2015 06:05:52 PM",317,5.22666550,32.36996078
......etc......
"""



import csv
from datetime import datetime, timedelta
import argparse
import math
import functools

def percentile(N, percent, key=lambda x:x):
    """
    Function stolen from: ## {{{ http://code.activestate.com/recipes/511478/ (r1)
    Find the percentile of a list of values.

    @parameter N - is a list of values. Note N MUST BE already sorted.
    @parameter percent - a float value from 0.0 to 1.0.
    @parameter key - optional key function to compute value from each element of N.

    @return - the percentile of the values
    """
    if not N:
        return None
    k = (len(N)-1) * percent
    f = math.floor(k)
    c = math.ceil(k)
    if f == c:
        return key(N[int(k)])
    d0 = key(N[int(f)]) * (c-k)
    d1 = key(N[int(c)]) * (k-f)
    return d0+d1


#Take in file name to sort and provide help
parser = argparse.ArgumentParser(description="""Script to take a CSV from CA's eHealth, detailing the bandwidth utilisation of an interface (bits in and bits out), and produce an output CSV containing the 95th percentile per day. The data format from eHealth is as follows:\n "eHealth Trend Report","logoRpt"\n "DEVICENAME-INTERFACE NAME"\n "Divide by Time"\n \n"","","Bandwidth Utilization In","Bandwidth Utilization Out"\n "Sample Time","Delta Time","%","%"\n "30/06/2015 06:00:35 PM",316,5.15666389,31.84219170\n "30/06/2015 06:05:52 PM",317,5.22666550,32.36996078\n ......etc......""")
parser.add_argument('input_file', help='A text file containing the output from a bits in/bits out interface trend report, in CSV format')
args=parser.parse_args()

in_file = open(args.input_file, 'rb') #reads the input file

in_file_csv = csv.reader(in_file) #process the input file as CSV

data = []
#You cannot remove items from a list in a for loop, so copy to new list
filtered_data = []

for row in in_file_csv:
    data.append(row) # add each line in CSV to list
for i in data:
    try:
        #try and turn the string date into a date object, and add 6 hours to make it GMT
        dateformatted = datetime.strptime(i[0], '%d/%m/%Y %I:%M:%S %p')
        dateformatted += timedelta(hours=6)
        i[0] = dateformatted
        filtered_data.append(i) # add it to the new filtered data list
    except:
        continue # don't raise exceptions - an exception will be raised for each line that doesn't start with a valid date/time. This also serves the purpose of removing headers and stuff

bwin = {}
bwout = {}

for i in filtered_data:
    this_date = '%s/%s/%s' % (i[0].day, i[0].month, i[0].year) # get the date as a string
    if this_date not in bwin: # make a new key, containing an empty list, if this is a new date
        bwin[this_date] = []
    bwin[this_date].append(i[2]) 
    if this_date not in bwout:
        bwout[this_date] = []
    bwout[this_date].append(i[3])

bwin95 = {}
bwout95 = {}

for key in bwin:
    bwin[key] = [int(round(float(x))) for x in bwin[key]] # Turn the string bandwidth value into an integer, after rounding
    bwin95[key] = percentile(sorted(bwin[key]), 0.95) # Get the 95th percentile and add to new list

for key in bwout:
    bwout[key] = [int(round(float(x))) for x in bwout[key]]

    bwout95[key] = percentile(sorted(bwout[key]), 0.95)

    

print bwout95
print ' '
print bwin95   
#Write it out
with open(args.input_file+'_95th_percentile.csv', 'w') as f:
    f.write('95th Percentil Values\n')
    f.write('Date,BWout,BWin\n')
    for key in bwout95:
        f.write(key+','+str(bwout95[key])+','+str(bwin95[key])+'\n')
    f.close()
